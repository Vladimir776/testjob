import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {WorkerData} from '../../intefaces/workers/worker-data';
import {Observable} from 'rxjs';
import {CreateWorkerResponce} from '../../intefaces/workers/create-worker-responce';
import {UpdateWorkerRespocne} from '../../intefaces/workers/update-worker-respocne';

@Injectable({
  providedIn: 'root'
})
export class WorkersService {
  readonly API_LINK = 'https://reqres.in/api/users/';
  constructor(
    private http: HttpClient
  ) { }

  create(name, job): Observable<CreateWorkerResponce> {
    return this.http.post<CreateWorkerResponce>(this.API_LINK, {name, job});
  }

  delete(id): Observable<any> {
    return this.http.delete(this.API_LINK + id);
  }

  update(id, newWorker: WorkerData): Observable<UpdateWorkerRespocne> {
    return this.http.put<UpdateWorkerRespocne>(this.API_LINK + id, {newWorker});
  }
}
