import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import { Observable, Subject} from 'rxjs';
import {tap} from 'rxjs/operators';
import {AuthService} from '../authService/auth.service';
import {NotificationService} from '../notificationService/notification.service';

/**
 * HttpHandlerService - перехватывает http запросы, чтобы выводить сообщения об ошибках пользователю
 */
@Injectable({
  providedIn: 'root'
})
export class HttpHandlerService implements HttpInterceptor {
  readonly errorSubject = new Subject<any>();
  private AUTH_HEADER = 'Authorization';
  private token = 'secrettoken';
  constructor(
    private  notification: NotificationService
  ) { }
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const authReq = req.clone({
      headers: req.headers.set('Session', '123456789')
    });
    return next.handle(authReq).pipe(
      tap(
        event => {
          console.log(event);
        },
        err => {
          this.notification.showError(err.error.error, 'Error');
          { console.log(err);
          }
        }
      )
    );
  }
}
