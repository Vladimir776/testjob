import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  readonly API_LINK = 'https://reqres.in/api';
  // tslint:disable-next-line:variable-name
  private _isLoggedIn = false;

  constructor(
    private http: HttpClient
  ) { }

  get isLoggedIn(){
    return this._isLoggedIn;
  }

  set isLoggedIn( login) {
    this._isLoggedIn = login;
  }

  logout() {
    this._isLoggedIn = false;
  }

  login(email, password) {
    return this.http.post(this.API_LINK + '/login', {email, password});
  }

  register(email, password) {
    return this.http.post(this.API_LINK + '/register', {email, password});
  }
}
