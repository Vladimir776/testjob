import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ResourceListResponse} from '../../intefaces/resouces/resource-list-response';
import {ResourceSingleResponse} from '../../intefaces/resouces/resource-single-response';

@Injectable({
  providedIn: 'root'
})

/**
 * Сервис получения ресурсов с @link https://reqres.in/
 * @method getResourceByPage
 * @param page - номер страницы с ресурсами.
 * @return Observable<ResourceListResponse> - обьект интерфейса Observable, позволяющий получать данные асинхронно.
 */
export class ResourcesService {
  readonly API_LINK = 'https://reqres.in/api/unknown/';

  constructor(
    private http: HttpClient
  ) {}

  getResourceByPage(page): Observable<ResourceListResponse> {
    return this.http.get<ResourceListResponse>(this.API_LINK + '?page=' + page);
  }

  getResourceById(id): Observable<ResourceSingleResponse> {
    return this.http.get<ResourceSingleResponse>(this.API_LINK  + id);
  }
}
