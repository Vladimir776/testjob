import {WorkerData} from './worker-data';

export interface UpdateWorkerRespocne extends WorkerData {
  updatedAt: string;
}
