export interface WorkerData {
  name: string;
  job: string;
  id?: number;
}
