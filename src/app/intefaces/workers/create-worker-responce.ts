import {WorkerData} from './worker-data';

export interface CreateWorkerResponce extends WorkerData {
  createdAt: string;
}
