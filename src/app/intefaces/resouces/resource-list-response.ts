import {ResourceData} from './resource-data';

/**
 * ResourceListResponse - интерфейс ответа от api сервера
 * @param page - номер страницы
 * @param per_page - кол-во элементов на страницу.
 * @param data -  данные в виде массива, интерфейса ResourceData
 */
export interface ResourceListResponse {
  page: number;
  per_page: number;
  total: number;
  total_pages: number;
  data: ResourceData[];
  ad: {
    company: string,
    url: string,
    text: string
  };
}
