import {ResourceData} from './resource-data';
export interface ResourceSingleResponse {
  data: ResourceData;
  ad: {
    company: string,
    url: string,
    text: string
  };
}
