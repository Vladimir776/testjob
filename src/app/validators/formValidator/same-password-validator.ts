import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';
export function samePassword(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors => {
    const fPass  = control.get('password');
    const sPass  = control.get('confirmPassword');
    if (fPass.value !==  sPass.value) {
      sPass.setErrors({notEquivalent: true});
    } else {
      sPass.setErrors(null);
    }
    return;
  };
}
