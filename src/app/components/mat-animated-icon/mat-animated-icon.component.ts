import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-mat-animated-icon',
  templateUrl: './mat-animated-icon.component.html',
  styleUrls: ['./mat-animated-icon.component.scss']
})
export class MatAnimatedIconComponent implements OnInit {
  @Input() start: string;
  @Input() end: string;
  @Input() colorStart: string;
  @Input() colorEnd: string;
  @Input() animate: boolean;
  @Input() animateFromParent = false;
  @Input() animateObserver: Observable<any>;

  constructor() { }

  ngOnInit(): void {
    this.animateObserver.subscribe(() => this.animate = false);
  }

  toggle() {
    if (!this.animateFromParent) { this.animate = !this.animate; }
  }
}
