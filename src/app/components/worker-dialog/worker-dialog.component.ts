import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {WorkerData} from '../../intefaces/workers/worker-data';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {WorkersService} from '../../services/workerService/workers.service';
import {NotificationService} from '../../services/notificationService/notification.service';
import {catchError, map, tap} from 'rxjs/operators';
import {from} from 'rxjs';
import {CreateWorkerResponce} from '../../intefaces/workers/create-worker-responce';
import {UpdateWorkerRespocne} from '../../intefaces/workers/update-worker-respocne';


/** Модальное окно для создания или редактирования данных
 * @param workersService - сервис делающий запрос на backend с получением данных оттуда
 * @param notificationService - сервис оповещения пользователя об успешной или
 * неуспешном выполнении запроса
 */
@Component({
  selector: 'app-worker-dialog',
  templateUrl: './worker-dialog.component.html',
  styleUrls: ['./worker-dialog.component.css', '../../../styles/loading.css']
})
export class WorkerDialogComponent implements OnInit {
  workerForm: FormGroup;
  // режим модального окна  true - создать обьект , false - редактировать.
  isCreate;
  // bool величена для spinner
  isComplete = false;
  worker: WorkerData = {name: '', job: ''};
  constructor(
    public dialogRef: MatDialogRef<WorkerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {workerData: WorkerData, isCreate: true},
    private fb: FormBuilder,
    private workersService: WorkersService,
    private notificationService: NotificationService
  ) {
    this.worker.name = data.workerData.name;
    this.worker.job = data.workerData.job;
    this.worker.id = data.workerData.id;
    this._createForm();
    this.isCreate = data.isCreate;
    this.workerForm.get('name').valueChanges.subscribe(value => this.worker.name = value);
    this.workerForm.get('job').valueChanges.subscribe(value => this.worker.job = value);
  }

  private _createForm() {
    this.workerForm = this.fb.group({
      name: [this.data.workerData.name, [Validators.required]],
      job: [this.data.workerData.job, [Validators.required]]
    });
  }
  ngOnInit(): void {
  }

  onNoClick() {
    this.dialogRef.close();
  }
  onSubmitClick() {
    this.isComplete = true;
    this.workersService.create(this.worker.name, this.worker.job).pipe(
      catchError((err) => {
        this.isComplete = false;
        this.notificationService.showError(err.message, 'Create Error');
        return from([]);
      }),
      tap<CreateWorkerResponce>((value) => {
        this.notificationService.showSuccess('User was created at ' + value.createdAt, 'Create success');
      })
    ).subscribe(value => {
        this.isComplete = false;
        //  копируем данные, избегаем ссылок
        this.worker.id = value.id;
        this.data.workerData.id = this.worker.id;
        this.data.workerData.name = this.worker.name;
        this.data.workerData.job = this.worker.job;
        this.dialogRef.close(this.data);
      }
    );
  }

  onEditClick() {
    this.isComplete = true;
    this.workersService.update(this.worker.id, this.worker).pipe(
      catchError((err) => {
        this.isComplete = false;
        this.notificationService.showError(err.message, 'Edit Error');
        return from([]);
      }),
      tap<UpdateWorkerRespocne>((value) => {
        this.notificationService.showSuccess('User was updated at ' + value.updatedAt, 'Update success');
      })
    ).subscribe(value => {
      this.isComplete = false;
      this.data.workerData.id = this.worker.id;
      this.data.workerData.name = this.worker.name;
      this.data.workerData.job = this.worker.job;
      this.dialogRef.close(this.data);
      }
    );
  }
}
