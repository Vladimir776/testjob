import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/authService/auth.service';
import {Router} from '@angular/router';
import {samePassword} from '../../validators/formValidator/same-password-validator';
import {catchError} from 'rxjs/operators';
import {from} from 'rxjs';


/** Реактивная форма для регистрации на сайте
 * @method _createForm  - реативная форма, созданая с помощью FormBuilder
 * @return registerForm - реактивная форма регистрации
 * @param authService - сервис авторизации и запросов на сервер
 * @param router - роутер для навигации по сайту
 * @param samePassword - валидатор для проверки одинаково заполненных полей пароль и подтвердите пароль
 */
@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.css', '../../../styles/authoriztionComponents.css',  '../../../styles/loading.css']
})
export class RegistrationPageComponent implements OnInit {
  registerForm: FormGroup;
  isLoginComplete = false;
  usersData = {email: '', password: ''};
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this._createForm();
    this.registerForm.get('email').valueChanges.subscribe(value => this.usersData.email = value);
    this.registerForm.get('password').valueChanges.subscribe(value => this.usersData.password = value);
  }

  private _createForm(){
    this.registerForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]],
    });
    this.registerForm.setValidators(samePassword());
  }
  ngOnInit(): void {
  }

  submit() {
    this.isLoginComplete = true;
    this.authService.register(this.usersData.email, this.usersData.password)
      .pipe(
        catchError(() => {
          // Убираем флаг spinner - загрузка не удалась
          this.isLoginComplete = false;
          return from([]);
        })
      )
      .subscribe(value => {
        this.authService.isLoggedIn = true;
        this.isLoginComplete = false;
        this.router.navigate(['table']);
      });
  }

  keySubmit($event: any) {
    if ($event.key === 'Enter' && this.registerForm.valid) { this.submit(); }
  }
}
