import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {WorkerData} from '../../intefaces/workers/worker-data';
import {MatDialog} from '@angular/material/dialog';
import {WorkerDialogComponent} from '../worker-dialog/worker-dialog.component';
import {WorkersService} from '../../services/workerService/workers.service';
import {NotificationService} from '../../services/notificationService/notification.service';
import {catchError} from 'rxjs/operators';
import {from} from 'rxjs';

@Component({
  selector: 'app-worker',
  templateUrl: './worker.component.html',
  styleUrls: ['./worker.component.css']
})
export class WorkerComponent implements OnInit {
  @Input() worker: WorkerData;
  @Output() deleteElement = new EventEmitter<WorkerData>();
  constructor(
    public dialog: MatDialog,
    private workersService: WorkersService,
    private notificationService: NotificationService
  ) { }

  openDialog(): void {
    const dialogRef = this.dialog.open(WorkerDialogComponent, {
      width: '250px',
      data: {workerData: this.worker, isCreate: false}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.worker = result.workerData;
    });
  }

  deleteWorker() {
    this.workersService.delete(this.worker.id).pipe(
      catchError((err) => {
        this.notificationService.showError(err.message, 'Delete Error');
        return from([]);
      })
    ).subscribe(value => {
      this.notificationService.showSuccess('id ' + this.worker.id + ' was successfully remove', 'Remove done' );
      this.deleteElement.emit(this.worker);
    });
  }
  ngOnInit(): void {
  }

}
