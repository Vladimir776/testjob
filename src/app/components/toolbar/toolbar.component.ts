import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {AuthService} from '../../services/authService/auth.service';

/**
 * @var triggerSideNavClose - обьект интерфейса Observable. Подписывается на событие закрытия бокового меню навигации
 * @var triggerSideNav - генерирует событие отрытия бокового меню навигации
 */
@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css'],
})
export class ToolbarComponent implements OnInit {
  @Input() triggerSideNavClose = new Observable<any>();
  @Output() triggerSideNav = new EventEmitter<any>();
  animate = false;
  constructor(
    private router: Router,
    private authService: AuthService
  ) {

  }

  ngOnInit(): void {
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['login']);
  }

  toTable() {
    this.router.navigate(['table']);
  }

  toWorkers() {
    this.router.navigate(['workers']);
  }
}
