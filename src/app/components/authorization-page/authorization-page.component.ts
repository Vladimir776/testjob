import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/authService/auth.service';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/operators';
import {from} from 'rxjs';


/** Реактивная форма валидации для входа на сайт
 * @method _createForm - форма, созданная с помощью FormBuilder
 * @return authForm - реактивная форма с параметрами email и password
 * @param authService - сервис авторизации и запросов на сервер
 * @param router - роутер для навигации по сайту
 */

@Component({
  selector: 'app-authorization-page',
  templateUrl: './authorization-page.component.html',
  styleUrls: ['./authorization-page.component.css', '../../../styles/authoriztionComponents.css', '../../../styles/loading.css']
})
export class AuthorizationPageComponent implements OnInit {
  hide = true;
  authForm: FormGroup;
  usersData = {email: '', password: ''};
  isLoginComplete = false;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this._createForm();
    this.authForm.get('email').valueChanges.subscribe(value => this.usersData.email = value);
    this.authForm.get('password').valueChanges.subscribe(value => this.usersData.password = value);
  }

  private _createForm() {
    this.authForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
  }

  submit() {
    this.isLoginComplete = true;
    this.authService.login(this.usersData.email, this.usersData.password)
      .pipe(
        catchError(() => {
          // Убираем флаг spinner - загрузка не удалась
          this.isLoginComplete = false;
          return from([]);
        })
      )
      .subscribe(value => {
        this.authService.isLoggedIn = true;
        this.isLoginComplete = false;
        this.router.navigate(['table']);
    });
  }
  navigateToRegisterPage() {
    this.router.navigate(['register']);
  }

  keySubmit($event) {
    if ($event.key === 'Enter' && this.authForm.valid) {
      this.submit();
    }
  }
}
