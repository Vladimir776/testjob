import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {WorkerDialogComponent} from '../worker-dialog/worker-dialog.component';
import {WorkerData} from '../../intefaces/workers/worker-data';
import {catchError, delay, map, mergeMap} from 'rxjs/operators';
import {from, Observable, of, Subject} from 'rxjs';
import {WorkersService} from '../../services/workerService/workers.service';

@Component({
  selector: 'app-workers-page',
  templateUrl: './workers-page.component.html',
  styleUrls: ['./workers-page.component.css', '../../../styles/loading.css']
})
export class WorkersPageComponent implements OnInit {
  // Выступает в роли псевдо-хранилища данных о работниках
  workers = new Map<number, WorkerData>();
  iter: Subject<Iterable<any>> = new Subject<Iterable<any>>();
  // bool величена для spinner
  isDelete = false;
  constructor(
    public dialog: MatDialog,
    private workersService: WorkersService
  ) { }

  openDialog(): void {
    const dialogRef = this.dialog.open(WorkerDialogComponent, {
      width: '250px',
      data: {workerData: {name: '', job: ''}, isCreate: true}
    });
    dialogRef.afterClosed()
      .pipe(
        mergeMap(value => {
          // Если просто закрыли модальное окно — ничего не добавляем.
          if (typeof value !== 'undefined' || !!value) {
          this.workers.set(value.workerData.id, value.workerData);
          }
          // делаем массив из значений мапы работников
          return of(Array.from(this.workers.values()));
        })
      ).subscribe(value => this.iter.next(value)
    );
  }

  ngOnInit(): void {
  }

  deleteWorker($event: WorkerData) {
     this.isDelete = true;
     this.workers.delete($event.id);
     this.workersService.delete($event.id)
       .pipe(
         catchError(value => {
           this.isDelete = false;
           return from([]);
         }),
         mergeMap(value => {
           this.isDelete = false;
           this.workers.delete($event.id);
           return of(Array.from(this.workers.values()));
         })
       )
       .subscribe(
         value => this.iter.next(value)
       );
  }
}
