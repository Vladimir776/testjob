import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {ResourcesService} from '../../services/resourcesService/resources.service';
import {ResourceData} from '../../intefaces/resouces/resource-data';
import {catchError, delay, map, startWith, switchMap} from 'rxjs/operators';
import {from, merge} from 'rxjs';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css', '../../../styles/authoriztionComponents.css', '../../../styles/loading.css']
})

export class TableComponent implements OnInit , AfterViewInit {
  displayedColumns: string[] = ['id', 'name', 'year', 'color', 'pantone_value'];
  dataSource = new MatTableDataSource<ResourceData>([]);
  isLoadingResults = true;
  constructor(private resourcesService: ResourcesService) {

  }
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  /**
   * @methodng AfterViewInit - инициализирует поток данных от сервера на таблицу
   */
  ngOnInit(): void {

  }
  ngAfterViewInit() {
    merge(this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          return this.resourcesService.getResourceByPage(this.paginator.pageIndex + 1);
        }),
        map(data => {
          this.paginator.length = data.total;
          this.paginator.pageSize = data.per_page;
          return data.data;
        }),
        catchError(() => {
          // Убираем флаг spinner - загрузка не удалась
          this.isLoadingResults = false;
          return from([]);
        })
      ).subscribe(value => {
      // Убираем флаг spinner - данные загрузились
      this.isLoadingResults = false;
      this.dataSource.data = value;
    });
  }
}


