import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/authService/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }


  logout() {
    this.authService.logout();
    this.router.navigate(['login']);
  }
  toTable() {
    this.router.navigate(['table']);
  }

  toWorkers() {
    this.router.navigate(['workers']);
  }
}
