import {Inject, Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from '../../services/authService/auth.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(@Inject(AuthService) private auth: AuthService, private router: Router){}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const auth =  this.auth.isLoggedIn;
    if (!auth) { this.router.navigate(['login']); }
    return auth;
  }

  canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const auth =  this.canActivate(next, state);
    if (!auth) { this.router.navigate(['login']); }
    return auth;
  }
}
