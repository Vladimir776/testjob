import { BrowserModule } from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {AngularMaterialModule} from './modules/angular-material/angular-material.module';
import { TableComponent } from './components/table/table.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatAnimatedIconComponent } from './components/mat-animated-icon/mat-animated-icon.component';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import {RouterModule} from '@angular/router';
import {AuthGuard} from './guards/routerGuard/authorization-guard.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { AuthorizationPageComponent } from './components/authorization-page/authorization-page.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpHandlerService} from './services/httpServiceHandler/http-handler.service';
import { RegistrationPageComponent } from './components/registration-page/registration-page.component';
import { MainPageComponent } from './components/main-page/main-page.component';
import { ToastrModule } from 'ngx-toastr';
import { WorkersPageComponent } from './components/workers-page/workers-page.component';
import { WorkerDialogComponent } from './components/worker-dialog/worker-dialog.component';
import { WorkerComponent } from './components/worker/worker.component';


@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    ToolbarComponent,
    MatAnimatedIconComponent,
    SideMenuComponent,
    AuthorizationPageComponent,
    RegistrationPageComponent,
    MainPageComponent,
    WorkersPageComponent,
    WorkerDialogComponent,
    WorkerComponent,
  ],
  entryComponents: [
    WorkerDialogComponent,
    WorkerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    FlexLayoutModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    RouterModule.forRoot([
      {path: 'login', component: AuthorizationPageComponent},
      {path: 'register', component: RegistrationPageComponent},
      {path: '', component: MainPageComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard] , children: [
          {path: 'table', component: TableComponent },
          {path: 'workers' , component: WorkersPageComponent}
        ] },
    ]),
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpHandlerService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
